package net.dwgload.de.shared

enum class StateProgressLoading {
    Loading,
    Successful
}