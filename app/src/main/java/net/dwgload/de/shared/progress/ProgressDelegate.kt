package net.dwgload.de.shared.progress

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import kotlin.properties.Delegates

class ProgressDelegate(
    private val fragmentManager: FragmentManager
) {

    var isVisible: Boolean by Delegates.observable(false) { _, _, newValue ->
        if (newValue) showProgress() else hideProgress()
    }

    private fun showProgress() {
        hideProgress()
        ProgressDialog.newInstance().show(fragmentManager, ProgressDialogTag)
    }

    private fun hideProgress() {
        (fragmentManager.findFragmentByTag(ProgressDialogTag) as? DialogFragment)?.dismiss()
            ?: run {
                fragmentManager.addFragmentOnAttachListener { _, fragment ->
                    if (!isVisible && fragment is DialogFragment) {
                        hideProgress()
                    }
                }
            }
    }

    companion object {
        const val ProgressDialogTag = "progress_dialog"
    }
}

fun lazyProgressDelegate(fragmentManager: () -> FragmentManager) =
    lazy(LazyThreadSafetyMode.SYNCHRONIZED) { ProgressDelegate(fragmentManager()) }