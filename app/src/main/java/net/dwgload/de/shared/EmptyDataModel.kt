package net.dwgload.de.shared

import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.airbnb.epoxy.EpoxyModel
import net.dwgload.de.R
import net.dwgload.de.databinding.ItemEmptyDataViewBinding

class EmptyDataModel(
    @DrawableRes private val icon: Int = R.drawable.ic_search,
    @StringRes private val value: Int = R.string.search_title
) : EpoxyModel<View>() {

    init {
        id(icon, value)
    }

    override fun getDefaultLayout(): Int = R.layout.item_empty_data_view

    override fun bind(view: View) {
        with(ItemEmptyDataViewBinding.bind(view)) {
            iconView.setImageDrawable(root.context.getDrawable(icon))
            titleView.setText(value)
        }
    }
}