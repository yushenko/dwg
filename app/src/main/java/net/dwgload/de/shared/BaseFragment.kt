package net.dwgload.de.shared

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import net.dwgload.de.shared.progress.ProgressDelegate
import net.dwgload.de.shared.progress.lazyProgressDelegate


abstract class BaseFragment<VB : ViewBinding>(@LayoutRes layoutId: Int) : Fragment(layoutId) {
    open var viewBinding: VB? = null
    val progressDelegate: ProgressDelegate by lazyProgressDelegate { childFragmentManager }
//    val errorDelegate: ErrorDialogDelegate by lazyErrorDelegate { requireContext() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewBinding = initViewBinding(view)
        super.onViewCreated(view, savedInstanceState)
        onInitFragment()
    }

    abstract fun initViewBinding(view: View): VB

    override fun onDestroy() {
        viewBinding = null
        super.onDestroy()
    }

    protected open fun onInitFragment() {
        viewBinding?.onInitViews()
        viewBinding?.onInitObserves()
    }

    protected open fun VB.onInitViews() {}
    protected open fun VB.onInitObserves() {}
}