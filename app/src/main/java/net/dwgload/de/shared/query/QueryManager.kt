package net.dwgload.de.shared.query

enum class QueryParam(val query: String) {
    All("c"),
    Artist("a"),
    Category("g"),
    Book("b"),
    News("n"),
    Album("c"),
    Player("c"),
    Collection("u"),
    Search("q"),
    Page("p")
}