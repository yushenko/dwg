package net.dwgload.de.shared

object Constant {
    const val IMAGE_URL = "https://load.dwgradio.net/de/data/redner/"
    const val START_PAGE = 1
}