package net.dwgload.de.shared

import androidx.annotation.StringRes
import java.io.IOException

sealed class AppException(message: String? = null): IOException(message) {
    class Id(@StringRes val id: Int) : AppException()
    class Message(val value: String) : AppException(value)
    object NoInternet : AppException()
    object Unexpected : AppException()
    object WrongCredentials : AppException()
}