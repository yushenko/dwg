package net.dwgload.de.shared.castom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.addTextChangedListener
import net.dwgload.de.R
import net.dwgload.de.shared.StateProgressLoading
import net.dwgload.de.shared.StateProgressLoading.Successful

class SearchView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var stateLoading: StateProgressLoading = Successful
        set(value) {
            field = value
            showState(value)
        }

    init {
        View.inflate(context, R.layout.view_search, this)
        obtainAttributes(context, attrs)
        initListeners()
    }

    private fun showState(stateLoading: StateProgressLoading) {
        findViewById<ProgressBar>(R.id.progressView).visibility = when (stateLoading) {
            StateProgressLoading.Loading -> View.VISIBLE
            Successful -> View.GONE
        }
        findViewById<AppCompatImageView>(R.id.searchIconView).visibility = when (stateLoading) {
            StateProgressLoading.Loading -> View.GONE
            Successful -> View.VISIBLE
        }
    }

    var text: String
        get() = findViewById<AppCompatEditText>(R.id.inputView).text.toString()
        set(value) {
            findViewById<AppCompatEditText>(R.id.inputView).setText(value)
        }

    var changed: (String) -> Unit = {}

    private fun initListeners() {
        findViewById<AppCompatImageView>(R.id.removeView).setOnClickListener {
            findViewById<EditText>(R.id.inputView).text.clear()
        }
        findViewById<AppCompatEditText>(R.id.inputView).addTextChangedListener {
            findViewById<AppCompatImageView>(R.id.removeView).visibility =
                if (it.toString().isNotEmpty()) View.VISIBLE else View.GONE
            changed(it.toString().trim())
        }
    }

    private fun obtainAttributes(context: Context, attrs: AttributeSet?) {
        val typedArray = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.SearchView,
            0, 0
        )
        try {
            with(typedArray) {
                findViewById<AppCompatEditText>(R.id.inputView).hint =
                    getString(R.styleable.SearchView_hint).orEmpty()
            }
        } finally {
            typedArray.recycle()
        }
    }
}