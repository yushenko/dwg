package net.dwgload.de.shared

import android.os.Build
import android.view.View
import androidx.annotation.ColorRes
import com.airbnb.epoxy.EpoxyModel
import net.dwgload.de.R
import net.dwgload.de.databinding.ItemLoadingBinding

class ItemLoadingModel(@ColorRes private val color: Int = R.color.colorPrimaryDark) :
    EpoxyModel<View>() {

    init {
        id(color)
    }

    override fun getDefaultLayout(): Int = R.layout.item_loading

    override fun bind(view: View) {
        with(ItemLoadingBinding.bind(view)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                progressView.indeterminateTintList =
                    root.context.getColorStateList(R.color.colorPrimaryDark)
            }
        }
    }
}