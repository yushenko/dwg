package net.dwgload.de.shared

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import net.dwgload.de.ext.doFinally

sealed class ViewState {
    object Idle: ViewState()
    object Loading: ViewState()
}

fun CoroutineScope.initLoading(data: MutableLiveData<ViewState>) {
    data.value = ViewState.Loading
    doFinally {
        data.value = ViewState.Idle
    }
}