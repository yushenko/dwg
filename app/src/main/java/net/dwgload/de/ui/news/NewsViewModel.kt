package net.dwgload.de.ui.news

import androidx.hilt.lifecycle.ViewModelInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import net.dwgload.de.data.models.news.TitleNews
import net.dwgload.de.data.models.news.toTitleNews
import net.dwgload.de.data.repository.TitleRepository
import net.dwgload.de.shared.CoroutineViewModel
import net.dwgload.de.shared.initLoading

class NewsViewModel @ViewModelInject constructor(
    private val repository: TitleRepository
) : CoroutineViewModel() {

    val titles = MutableStateFlow<List<TitleNews>?>(listOf())
    val isPlaying = MutableStateFlow(false)

    init {
        loadTitles()
    }

    fun updateData() {
        loadTitles()
    }

    private fun loadTitles() = launch {
        initLoading(viewState)
        titles.value = repository.getNews().titles
            .map { it.toTitleNews() }
    }

    fun playerAnimation(isPlay: Boolean) {
        isPlaying.value = isPlay
    }
}