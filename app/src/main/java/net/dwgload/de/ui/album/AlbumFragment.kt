package net.dwgload.de.ui.album

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.idapgroup.lifecycle.ktx.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import net.dwgload.de.R
import net.dwgload.de.databinding.FragmentAlbumBinding
import net.dwgload.de.ext.addScrollListener
import net.dwgload.de.shared.BaseFragment
import net.dwgload.de.shared.ViewState
import net.dwgload.de.shared.query.QueryParam

@AndroidEntryPoint
class AlbumFragment : BaseFragment<FragmentAlbumBinding>(R.layout.fragment_album) {

    private val viewModel: AlbumViewModel by viewModels()

    private val name: String by lazy {
        AlbumFragmentArgs.fromBundle(requireArguments()).name
    }

    private val type: QueryParam by lazy {
        AlbumFragmentArgs.fromBundle(requireArguments()).type
    }

    private val titleId: Int by lazy {
        AlbumFragmentArgs.fromBundle(requireArguments()).titleId
    }

    private val controller = AlbumController {
        findNavController().navigate(
            AlbumFragmentDirections.openNewPlayerFragment(it.mainTitle ?: "", it.albumId, it.id)
        )
    }

    override fun FragmentAlbumBinding.onInitViews() {
        labelView.text = name
        backView.setOnClickListener {
            findNavController().popBackStack()
        }
        searchView.setOnClickListener {
            findNavController().navigate(AlbumFragmentDirections.openSearchFragment())
        }
        recyclerView.adapter = controller.adapter
        recyclerView.addScrollListener {
            viewModel.nextPage()
        }
        viewModel.startPage(type, titleId)
    }

    override fun FragmentAlbumBinding.onInitObserves() {
        with(viewModel) {
            observe(viewState) {
                progressDelegate.isVisible = it == ViewState.Loading
            }
            titles.filterNotNull().onEach {
                controller.setData(it)
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }

    }

    override fun initViewBinding(view: View) = FragmentAlbumBinding.bind(view)

}