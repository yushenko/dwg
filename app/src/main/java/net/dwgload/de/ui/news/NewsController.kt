package net.dwgload.de.ui.news

import android.view.View
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.TypedEpoxyController
import net.dwgload.de.R
import net.dwgload.de.data.models.news.TitleNews
import net.dwgload.de.databinding.ItemNewsBinding

class NewsController(
    val onClick: (TitleNews) -> Unit,
    val onDetails: (TitleNews) -> Unit
) :
    TypedEpoxyController<List<TitleNews>>() {

    override fun buildModels(data: List<TitleNews>?) {
        data?.forEach { item ->
            NewsModel(item).addTo(this)
        }
    }

    inner class NewsModel(private val data: TitleNews) : EpoxyModel<View>() {

        init {
            id(data.hashCode())
        }

        override fun getDefaultLayout(): Int = R.layout.item_news

        override fun bind(view: View) {
            with(ItemNewsBinding.bind(view)) {
                titleView.text = data.title
                themeView.text = data.genres
                artistView.text = data.artistName
                passageView.text = data.passageBook
                countView.text = data.titlesCount.toString()

                themeView.visibility =
                    if (data.genres.trim().isNotEmpty()) View.VISIBLE else View.GONE
                artistView.visibility =
                    if (data.artistName.trim().isNotEmpty()) View.VISIBLE else View.GONE
                passageView.visibility =
                    if (data.passageBook.isNotEmpty()) View.VISIBLE else View.GONE
                countView.visibility = if (data.titlesCount > 1) View.VISIBLE else View.GONE

                root.setOnClickListener {
                    if (data.titlesCount <= 1) onClick(data) else onDetails(data)
                }

            }
        }
    }
}