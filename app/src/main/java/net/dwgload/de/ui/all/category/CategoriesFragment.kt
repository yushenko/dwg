package net.dwgload.de.ui.all.category

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.idapgroup.lifecycle.ktx.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import net.dwgload.de.R
import net.dwgload.de.databinding.FragmentCategoriesBinding
import net.dwgload.de.shared.BaseFragment
import net.dwgload.de.shared.ViewState
import net.dwgload.de.shared.query.QueryParam
import net.dwgload.de.ui.all.AllFragmentDirections

@AndroidEntryPoint
class CategoriesFragment :
    BaseFragment<FragmentCategoriesBinding>(R.layout.fragment_categories),
    SwipeRefreshLayout.OnRefreshListener {

    private val viewModel: CategoriesViewModel by viewModels()

    private val controller = CategoriesController {
        findNavController().navigate(
            AllFragmentDirections.openDetailsFragment(it.name, QueryParam.Category, it.id)
        )
    }

    override fun FragmentCategoriesBinding.onInitViews() {
        recyclerView.adapter = controller.adapter
        refreshLayout.setOnRefreshListener(this@CategoriesFragment)
    }

    override fun FragmentCategoriesBinding.onInitObserves() {
        with(viewModel) {
            observe(viewState) {
                progressDelegate.isVisible = it == ViewState.Loading
                refreshLayout.isRefreshing = false
            }
            categories.filterNotNull().onEach {
                controller.setData(it)
            }
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    override fun onRefresh() {
        viewModel.updateData()
    }

    override fun initViewBinding(view: View) = FragmentCategoriesBinding.bind(view)
}