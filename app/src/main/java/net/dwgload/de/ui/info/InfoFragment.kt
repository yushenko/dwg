package net.dwgload.de.ui.info

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import dagger.hilt.android.AndroidEntryPoint
import net.dwgload.de.R
import net.dwgload.de.databinding.FragmentInfoBinding
import net.dwgload.de.shared.BaseFragment
import net.dwgload.de.ui.info.InfoFragment.Companion.Email


@AndroidEntryPoint
class InfoFragment : BaseFragment<FragmentInfoBinding>(R.layout.fragment_info) {

    override fun FragmentInfoBinding.onInitViews() {
        emailView.setOnClickListener {
            val emailIntent = Intent(Intent.ACTION_SENDTO)
            emailIntent.data = Uri.parse("mailto:$Email")
            startActivity(emailIntent)
        }

        developerView.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("https://t.me/DWG_app_bot")
            startActivity(intent)
        }

        loadView.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(DwgLoad)
            startActivity(intent)
        }

        radioView.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(DwgRadio)
            startActivity(intent)
        }

        payPalView.setOnClickListener {
            val view = LayoutInflater.from(context).inflate(R.layout.layout_paypal, null)
            val dialog: AlertDialog = AlertDialog.Builder(context).setView(view).create()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            view.findViewById<TextView>(R.id.tvCancel).setOnClickListener { dialog.dismiss() }
            view.findViewById<TextView>(R.id.tvCopy).setOnClickListener {
                copyPayPalInfo()
                dialog.dismiss()
            }
            dialog.show()
        }
    }

    private fun copyPayPalInfo() {
        val clipboard = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
        val clip = ClipData.newPlainText("Copied text", getString(R.string.email_text))
        clipboard?.setPrimaryClip(clip)
        Toast.makeText(context, getString(R.string.paypal_copied), Toast.LENGTH_SHORT).show()
    }

    override fun initViewBinding(view: View) = FragmentInfoBinding.bind(view)

    companion object {
        const val Email = "finanz@dwgradio.de"
        const val DwgLoad = "https://load.dwgradio.net"
        const val DwgRadio = "https://radio.dwgradio.net"
    }
}