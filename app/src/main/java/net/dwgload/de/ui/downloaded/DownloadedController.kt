package net.dwgload.de.ui.downloaded

import android.view.View
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.TypedEpoxyController
import net.dwgload.de.R
import net.dwgload.de.data.models.title.Title
import net.dwgload.de.databinding.ItemTitleDetailsBinding
import net.dwgload.de.shared.EmptyDataModel

class DownloadedController(
    val onClick: (Title) -> Unit
) : TypedEpoxyController<List<Title>>() {

    override fun buildModels(data: List<Title>?) {
        if (data?.size == 0) EmptyDataModel(R.drawable.ic_download, R.string.empty_list_downloaded).addTo(this)
        data?.forEach { item ->
            NewsModel(item).addTo(this)
        }
    }

    inner class NewsModel(private val data: Title) : EpoxyModel<View>() {

        init {
            id(data.hashCode())
        }

        override fun getDefaultLayout(): Int = R.layout.item_downloaded

        override fun bind(view: View) {
            with(ItemTitleDetailsBinding.bind(view)) {
                titleView.text = data.mainTitle
                themeView.text = data.genres
                artistView.text = data.artistName
                passageView.text = data.passageBook
                countView.text = data.titlesCount.toString()

                themeView.visibility =
                    if (data.genres.trim().isNotEmpty()) View.VISIBLE else View.GONE
                artistView.visibility =
                    if (data.artistName.trim().isNotEmpty()) View.VISIBLE else View.GONE
                passageView.visibility =
                    if (data.passageBook.isNotEmpty()) View.VISIBLE else View.GONE
                countView.visibility = if (data.titlesCount > 1) View.VISIBLE else View.GONE

                root.setOnClickListener {
                    onClick(data)
                }

            }
        }
    }
}