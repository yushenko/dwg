package net.dwgload.de.ui.all.artist

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.idapgroup.lifecycle.ktx.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_downloaded.*
import kotlinx.android.synthetic.main.layout_progress.*
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import net.dwgload.de.R
import net.dwgload.de.databinding.FragmentArtistsBinding
import net.dwgload.de.shared.BaseFragment
import net.dwgload.de.shared.ViewState
import net.dwgload.de.shared.query.QueryParam
import net.dwgload.de.ui.all.AllFragmentDirections


@AndroidEntryPoint
class ArtistsFragment :
    BaseFragment<FragmentArtistsBinding>(R.layout.fragment_artists),
    SwipeRefreshLayout.OnRefreshListener {

    private val viewModel: ArtistsViewModel by viewModels()

    private val controller = ArtistsController {
        findNavController().navigate(
            AllFragmentDirections.openDetailsFragment(it.name, QueryParam.Artist, it.id)
        )
    }

    override fun FragmentArtistsBinding.onInitViews() {
        recyclerView.adapter = controller.adapter
        refreshLayout.setOnRefreshListener(this@ArtistsFragment)
    }

    override fun FragmentArtistsBinding.onInitObserves() {
        with(viewModel) {
            observe(viewState) {
                progressDelegate.isVisible = it == ViewState.Loading
                refreshLayout.isRefreshing = false
            }
            artists.filterNotNull().onEach {
                controller.setData(it)
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }
    }

    override fun onRefresh() {
        viewModel.updateData()
    }

    override fun initViewBinding(view: View) = FragmentArtistsBinding.bind(view)

}