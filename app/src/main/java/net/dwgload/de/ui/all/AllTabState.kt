package net.dwgload.de.ui.all

import androidx.annotation.StringRes
import net.dwgload.de.R

enum class AllTabState(
    @StringRes val title: Int
) {
    All(R.string.all),
    Artist(R.string.artist),
    Category(R.string.category),
    Book(R.string.book);

    companion object {
        fun byPosition(position: Int) = values()[position]
    }
}