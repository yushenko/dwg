package net.dwgload.de.ui.all.title

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.idapgroup.lifecycle.ktx.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_downloaded.*
import kotlinx.android.synthetic.main.layout_progress.*
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import net.dwgload.de.R
import net.dwgload.de.databinding.FragmentTitlesBinding
import net.dwgload.de.ext.addScrollListener
import net.dwgload.de.shared.BaseFragment
import net.dwgload.de.shared.ViewState
import net.dwgload.de.shared.query.QueryParam
import net.dwgload.de.ui.all.AllFragmentDirections

@AndroidEntryPoint
class TitlesFragment :
    BaseFragment<FragmentTitlesBinding>(R.layout.fragment_titles),
    SwipeRefreshLayout.OnRefreshListener {

    private val viewModel: TitlesViewModel by viewModels()

    private val controller = TitlesController(
        onClick = {
            findNavController().navigate(
                AllFragmentDirections.openPlayerFragment(it.title, it.albumId, it.id)
            )
        },
        onDetails = {
            findNavController().navigate(
                AllFragmentDirections.openAlbumFragment(it.title, QueryParam.Album, it.albumId)
            )
        }
    )

    override fun FragmentTitlesBinding.onInitViews() {
        recyclerView.adapter = controller.adapter
        recyclerView.addScrollListener {
            viewModel.nextPage()
        }
        refreshLayout.setOnRefreshListener(this@TitlesFragment)
        viewModel.startPage()
    }

    override fun FragmentTitlesBinding.onInitObserves() {
        with(viewModel) {
            observe(viewState) {
                progressDelegate.isVisible = it == ViewState.Loading
                refreshLayout.isRefreshing = false
            }
            titles.filterNotNull().onEach {
                controller.setData(it)
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }
    }

    override fun onRefresh() {
        viewModel.updateData()
    }

    override fun initViewBinding(view: View) = FragmentTitlesBinding.bind(view)
}