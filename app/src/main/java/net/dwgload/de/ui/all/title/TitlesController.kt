package net.dwgload.de.ui.all.title

import android.util.Log
import android.view.View
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.TypedEpoxyController
import net.dwgload.de.R
import net.dwgload.de.data.models.all.TitleAll
import net.dwgload.de.databinding.ItemTitleBinding

class TitlesController(
    val onClick: (TitleAll) -> Unit,
    val onDetails: (TitleAll) -> Unit
) : TypedEpoxyController<List<TitleAll>>() {

    override fun buildModels(data: List<TitleAll>?) {
        data?.forEach { item ->
            TitlesModel(item).addTo(this)
        }
    }

    inner class TitlesModel(private val data: TitleAll) : EpoxyModel<View>() {

        init {
            id(data.hashCode())
        }

        override fun getDefaultLayout(): Int = R.layout.item_title

        override fun bind(view: View) {
            with(ItemTitleBinding.bind(view)) {
                titleView.text = data.title
                themeView.text = data.genres
                artistView.text = data.artistName
                passageView.text = data.passageBook
                countView.text = data.track.toString()

                Log.i("TAG", "ID = ${data.albumId}")

                themeView.visibility =
                    if (data.genres.trim().isNotEmpty()) View.VISIBLE else View.GONE
                artistView.visibility =
                    if (data.artistName.trim().isNotEmpty()) View.VISIBLE else View.GONE
                passageView.visibility =
                    if (data.passageBook.isNotEmpty()) View.VISIBLE else View.GONE
                countView.visibility = if (data.track > 1) View.VISIBLE else View.GONE

                root.setOnClickListener {
                    if (data.track <= 1) onClick(data) else onDetails(data)
                }

            }
        }
    }
}