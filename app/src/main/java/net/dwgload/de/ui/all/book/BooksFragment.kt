package net.dwgload.de.ui.all.book

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.idapgroup.lifecycle.ktx.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import net.dwgload.de.R
import net.dwgload.de.databinding.FragmentBooksBinding
import net.dwgload.de.shared.BaseFragment
import net.dwgload.de.shared.ViewState
import net.dwgload.de.shared.query.QueryParam
import net.dwgload.de.ui.all.AllFragmentDirections

@AndroidEntryPoint
class BooksFragment :
    BaseFragment<FragmentBooksBinding>(R.layout.fragment_books),
    SwipeRefreshLayout.OnRefreshListener {

    private val viewModel: BooksViewModel by viewModels()

    private val controller = BooksController {
        findNavController().navigate(
            AllFragmentDirections.openDetailsFragment(it.name, QueryParam.Book, it.id)
        )
    }

    override fun FragmentBooksBinding.onInitViews() {
        recyclerView.adapter = controller.adapter
        refreshLayout.setOnRefreshListener(this@BooksFragment)
    }

    override fun FragmentBooksBinding.onInitObserves() {
        with(viewModel) {
            observe(viewState) {
                progressDelegate.isVisible = it == ViewState.Loading
                refreshLayout.isRefreshing = false
            }
            books.filterNotNull().onEach {
                controller.setData(it)
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }
    }

    override fun onRefresh() {
        viewModel.updateData()
    }

    override fun initViewBinding(view: View) = FragmentBooksBinding.bind(view)
}