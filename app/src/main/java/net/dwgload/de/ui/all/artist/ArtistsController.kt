package net.dwgload.de.ui.all.artist

import android.view.View
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.TypedEpoxyController
import com.bumptech.glide.Glide
import net.dwgload.de.R
import net.dwgload.de.data.models.artist.Artist
import net.dwgload.de.databinding.ItemArtistBinding

class ArtistsController(val onClick: (Artist) -> Unit) : TypedEpoxyController<List<Artist>>() {

    override fun buildModels(data: List<Artist>?) {
        data?.forEach { item ->
            CollectionModel(item).addTo(this)
        }
    }

    inner class CollectionModel(private val data: Artist) : EpoxyModel<View>() {

        init {
            id(data.hashCode())
        }

        override fun getDefaultLayout(): Int = R.layout.item_artist

        override fun bind(view: View) {
            with(ItemArtistBinding.bind(view)) {
                titleView.text = data.name
                Glide.with(artistView.context).load(data.image).error(R.drawable.ic_place_holder)
                    .into(artistView)
                countView.text = data.count.toString()
                root.setOnClickListener {
                    onClick(data)
                }
            }
        }
    }
}