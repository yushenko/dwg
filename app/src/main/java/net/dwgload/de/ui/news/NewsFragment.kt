package net.dwgload.de.ui.news

import android.content.Context
import android.content.Intent
import android.support.v4.media.session.PlaybackStateCompat
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.idapgroup.lifecycle.ktx.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_all.*
import kotlinx.android.synthetic.main.layout_app_bar.*
import kotlinx.android.synthetic.main.layout_progress.*
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import net.dwgload.de.R
import net.dwgload.de.data.services.PlayerService
import net.dwgload.de.databinding.FragmentNewsBinding
import net.dwgload.de.shared.BaseFragment
import net.dwgload.de.shared.ViewState
import net.dwgload.de.shared.query.QueryParam
import net.dwgload.de.ui.player.player.AudioController

@AndroidEntryPoint
class NewsFragment :
    BaseFragment<FragmentNewsBinding>(R.layout.fragment_news),
    SwipeRefreshLayout.OnRefreshListener {

    private val viewModel: NewsViewModel by viewModels()


    private val callback = object : AudioController() {
        override fun getContext() = requireActivity()

        override fun playbackStateChange(state: PlaybackStateCompat?) {
            when (state?.state == PlaybackStateCompat.STATE_PLAYING) {
                true -> {
//                    requireActivity().findViewById<LottieAnimationView>(R.id.playView)
//                        .playAnimation()
                }
                false -> {
//                    requireActivity().findViewById<LottieAnimationView>(R.id.playView)
//                        .pauseAnimation()
                }
            }
        }
    }

    private val controller = NewsController(
        onClick = {
            findNavController().navigate(
                NewsFragmentDirections.openNewPlayerFragment(it.title, it.albumId, it.id)
            )
        },
        onDetails = {
            findNavController().navigate(
                NewsFragmentDirections.openAlbumFragment(it.title, QueryParam.Album, it.albumId)
            )
        }
    )

    override fun FragmentNewsBinding.onInitViews() {
        recyclerView.adapter = controller.adapter
        refreshLayout.setOnRefreshListener(this@NewsFragment)

        searchView.setOnClickListener {
            findNavController().navigate(NewsFragmentDirections.openSearchFragment())
        }

        requireActivity().bindService(
            Intent(requireContext(), PlayerService::class.java),
            callback.getServiceConnection(),
            Context.BIND_AUTO_CREATE
        )
    }

    override fun FragmentNewsBinding.onInitObserves() {
        with(viewModel) {
            observe(viewState) {
                progressDelegate.isVisible = it == ViewState.Loading
                refreshLayout.isRefreshing = false
            }
            isPlaying.onEach {
                //playingInfo(it)
            }.launchIn(viewLifecycleOwner.lifecycleScope)
            titles.filterNotNull().onEach {
                controller.setData(it)
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }
    }

    override fun onDestroy() {
        callback.disconnect()
        requireActivity().unbindService(callback.getServiceConnection())
        super.onDestroy()
    }

    private fun FragmentNewsBinding.playingInfo(isPlay: Boolean) {
//        when (isPlay) {
//            true -> {
//                playerView.playAnimation()
//            }
//            false -> {
//                playerView.pauseAnimation()
//            }
//        }
    }

    override fun onRefresh() {
        viewModel.updateData()
    }

    override fun initViewBinding(view: View) = FragmentNewsBinding.bind(view)
}