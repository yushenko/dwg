package net.dwgload.de.ui.details

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.idapgroup.lifecycle.ktx.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import net.dwgload.de.R
import net.dwgload.de.databinding.FragmentDetailsBinding
import net.dwgload.de.ext.addScrollListener
import net.dwgload.de.shared.BaseFragment
import net.dwgload.de.shared.ViewState
import net.dwgload.de.shared.query.QueryParam

@AndroidEntryPoint
class DetailsFragment :
    BaseFragment<FragmentDetailsBinding>(R.layout.fragment_details) {

    private val viewModel: DetailsViewModel by viewModels()

    private val name: String by lazy {
        DetailsFragmentArgs.fromBundle(requireArguments()).name
    }

    private val type: QueryParam by lazy {
        DetailsFragmentArgs.fromBundle(requireArguments()).type
    }

    private val titleId: Int by lazy {
        DetailsFragmentArgs.fromBundle(requireArguments()).titleId
    }

    private val controller = DetailsController(
        onClick = {
            findNavController().navigate(
                DetailsFragmentDirections.openPlayerFragment(it.mainTitle, it.albumId, it.id)
            )
        },
        onDetails = {
            findNavController().navigate(
                DetailsFragmentDirections.openAlbumFragment(it.mainTitle, QueryParam.Album, it.albumId)
            )
        }
    )

    override fun FragmentDetailsBinding.onInitViews() {
        labelView.text = name
        backView.setOnClickListener {
            findNavController().popBackStack()
        }
        searchView.setOnClickListener {
            findNavController().navigate(DetailsFragmentDirections.openSearchFragment())
        }
        recyclerView.adapter = controller.adapter
        recyclerView.addScrollListener {
            viewModel.nextPage()
        }
        viewModel.startPage(type, titleId)
    }

    override fun FragmentDetailsBinding.onInitObserves() {
        with(viewModel) {
            observe(viewState) {
                progressDelegate.isVisible = it == ViewState.Loading
            }
            titles.filterNotNull().onEach {
                controller.setData(it)
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }
    }

    override fun initViewBinding(view: View) = FragmentDetailsBinding.bind(view)
}