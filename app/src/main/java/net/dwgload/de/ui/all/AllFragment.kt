package net.dwgload.de.ui.all

import android.view.View
import androidx.navigation.fragment.findNavController
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import net.dwgload.de.R
import net.dwgload.de.databinding.FragmentAllBinding
import net.dwgload.de.shared.BaseFragment

@AndroidEntryPoint
class AllFragment : BaseFragment<FragmentAllBinding>(R.layout.fragment_all) {

    override fun FragmentAllBinding.onInitViews() {
        searchView.setOnClickListener {
            findNavController().navigate(AllFragmentDirections.openSearchFragment())
        }
        val adapter = AllPageAdapter(this@AllFragment)
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 1

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text =
                getString(AllTabState.byPosition(position).title)
        }.attach()
    }

    override fun initViewBinding(view: View) = FragmentAllBinding.bind(view)
}