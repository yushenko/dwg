package net.dwgload.de.ui.downloaded

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import net.dwgload.de.R
import net.dwgload.de.databinding.FragmentDownloadedBinding
import net.dwgload.de.shared.BaseFragment
import net.dwgload.de.ui.news.NewsFragmentDirections

@AndroidEntryPoint
class DownloadedFragment : BaseFragment<FragmentDownloadedBinding>(R.layout.fragment_downloaded) {

    private val viewModel: DownloadedViewModel by viewModels()

    private val controller = DownloadedController {

    }

    override fun FragmentDownloadedBinding.onInitViews() {
        searchView.setOnClickListener {
            findNavController().navigate(NewsFragmentDirections.openSearchFragment())
        }

        recyclerView.adapter = controller.adapter
    }

    override fun FragmentDownloadedBinding.onInitObserves() {
        with(viewModel) {
            album.filterNotNull().onEach {
                controller.setData(it)
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }
    }

    override fun initViewBinding(view: View) = FragmentDownloadedBinding.bind(view)

}