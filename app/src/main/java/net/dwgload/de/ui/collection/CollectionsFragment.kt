package net.dwgload.de.ui.collection

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.idapgroup.lifecycle.ktx.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import net.dwgload.de.R
import net.dwgload.de.databinding.FragmentCollectionsBinding
import net.dwgload.de.shared.BaseFragment
import net.dwgload.de.shared.ViewState
import net.dwgload.de.shared.query.QueryParam

@AndroidEntryPoint
class CollectionsFragment :
    BaseFragment<FragmentCollectionsBinding>(R.layout.fragment_collections) {

    private val viewModel: CollectionsViewModel by viewModels()

    private val controller = CollectionController {
        findNavController().navigate(
            CollectionsFragmentDirections.openDetailsFragment(it.name, QueryParam.Collection, it.id)
        )
    }

    override fun FragmentCollectionsBinding.onInitViews() {
        searchView.setOnClickListener {
            findNavController().navigate(CollectionsFragmentDirections.openSearchFragment())
        }
        recycleView.adapter = controller.adapter
    }

    override fun FragmentCollectionsBinding.onInitObserves() {
        with(viewModel) {
            observe(viewState) { progressDelegate.isVisible = it == ViewState.Loading }
            collections.filterNotNull().onEach {
                controller.setData(it)
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }
    }

    override fun initViewBinding(view: View) = FragmentCollectionsBinding.bind(view)
}