package net.dwgload.de.ui.all.category

import android.view.View
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.TypedEpoxyController
import net.dwgload.de.R
import net.dwgload.de.data.models.category.Category
import net.dwgload.de.databinding.ItemCategoryBinding

class CategoriesController(val onClick: (Category) -> Unit) :
    TypedEpoxyController<List<Category>>() {

    override fun buildModels(data: List<Category>?) {
        data?.forEach { item ->
            CategoryModel(item).addTo(this)
        }
    }

    inner class CategoryModel(private val data: Category) : EpoxyModel<View>() {

        init {
            id(data.hashCode())
        }

        override fun getDefaultLayout(): Int = R.layout.item_category

        override fun bind(view: View) {
            ItemCategoryBinding.bind(view).apply {
                titleView.text = data.name
                countView.text = data.count.toString()
                root.setOnClickListener {
                    onClick(data)
                }
            }
        }
    }
}