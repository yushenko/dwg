package net.dwgload.de.ui.all.book

import android.view.View
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.TypedEpoxyController
import net.dwgload.de.R
import net.dwgload.de.data.models.book.Book
import net.dwgload.de.databinding.ItemBookBinding

class BooksController(val onClick: (Book) -> Unit) :
    TypedEpoxyController<List<Book>>() {

    override fun buildModels(data: List<Book>?) {
        data?.forEach { item ->
            BooksModel(item).addTo(this)
        }
    }

    inner class BooksModel(private val data: Book) : EpoxyModel<View>() {

        init {
            id(data.hashCode())
        }

        override fun getDefaultLayout(): Int = R.layout.item_book

        override fun bind(view: View) {
            with(ItemBookBinding.bind(view)) {
                titleView.text = data.name
                countView.text = data.count.toString()
                root.setOnClickListener {
                    onClick(data)
                }
            }
        }
    }
}