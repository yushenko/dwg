package net.dwgload.de.ui.all.category

import androidx.hilt.lifecycle.ViewModelInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import net.dwgload.de.data.models.category.Category
import net.dwgload.de.data.models.category.toCategory
import net.dwgload.de.data.repository.TitleRepository
import net.dwgload.de.shared.CoroutineViewModel
import net.dwgload.de.shared.initLoading

class CategoriesViewModel @ViewModelInject constructor(
    private val repository: TitleRepository
) : CoroutineViewModel() {

    val categories = MutableStateFlow<List<Category>>(listOf())

    init {
        loadCategory()
    }

    fun updateData() {
        loadCategory()
    }

    private fun loadCategory() = launch {
        initLoading(viewState)
        categories.value = repository.getCategories().map { it.toCategory() }
    }
}