package net.dwgload.de.ui.collection

import androidx.hilt.lifecycle.ViewModelInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import net.dwgload.de.data.models.collection.Collection
import net.dwgload.de.data.models.collection.toCollection
import net.dwgload.de.data.repository.TitleRepository
import net.dwgload.de.shared.CoroutineViewModel
import net.dwgload.de.shared.initLoading

class CollectionsViewModel @ViewModelInject constructor(
    private val repository: TitleRepository
) : CoroutineViewModel() {

    val collections = MutableStateFlow<List<Collection>>(listOf())

    init {
        launch {
            initLoading(viewState)
            collections.value = repository.getCollections().map { it.toCollection() }
        }
    }

}