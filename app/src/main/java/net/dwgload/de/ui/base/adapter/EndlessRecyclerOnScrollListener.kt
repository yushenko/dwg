package net.dwgload.de.ui.base.adapter

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class EndlessRecyclerOnScrollListener(private val linearLayoutManager: LinearLayoutManager?) :
    RecyclerView.OnScrollListener() {

    var firstVisibleItem: Int = 0
    var lastVisibleItem: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    var isNeedLoadNextPage: Boolean = true

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        visibleItemCount = recyclerView.childCount
        totalItemCount = linearLayoutManager?.itemCount ?: 0
        firstVisibleItem = linearLayoutManager?.findFirstVisibleItemPosition() ?: 0

        if (isNeedLoadNextPage && totalItemCount - visibleItemCount - 3 <= firstVisibleItem && lastVisibleItem < firstVisibleItem) {
            onLoadMore()
            isNeedLoadNextPage = false
        }
        lastVisibleItem = linearLayoutManager?.findFirstVisibleItemPosition() ?: 0
    }

    abstract fun onLoadMore()
}