package net.dwgload.de.ui.all

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import net.dwgload.de.ui.all.artist.ArtistsFragment
import net.dwgload.de.ui.all.book.BooksFragment
import net.dwgload.de.ui.all.category.CategoriesFragment
import net.dwgload.de.ui.all.title.TitlesFragment

class AllPageAdapter(fm: Fragment) :
    FragmentStateAdapter(fm) {

    override fun getItemCount(): Int = AllTabState.values().size

    override fun createFragment(position: Int): Fragment =
        when (AllTabState.byPosition(position)) {
            AllTabState.All -> TitlesFragment()
            AllTabState.Artist -> ArtistsFragment()
            AllTabState.Category -> CategoriesFragment()
            AllTabState.Book -> BooksFragment()
        }
}