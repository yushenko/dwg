package net.dwgload.de.ui.search

import androidx.hilt.lifecycle.ViewModelInject
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import net.dwgload.de.data.models.search.TitleSearch
import net.dwgload.de.data.models.search.toTitleSearch
import net.dwgload.de.data.repository.TitleRepository
import net.dwgload.de.shared.CoroutineViewModel
import net.dwgload.de.shared.initLoading
import net.dwgload.de.shared.Constant

class SearchViewModel @ViewModelInject constructor(
    var repository: TitleRepository
) : CoroutineViewModel() {

    val titles = MutableStateFlow<Pair<String, List<TitleSearch>>>("" to listOf())
    val titleCount = MutableStateFlow(0)
    private val query = MutableStateFlow("")
    val page = MutableStateFlow(0)
    private var count: Int = 0


    init {
        observeQueryChanges()
    }

    private fun observeQueryChanges() {
        page.onEach {
            getTitles(query.value, it)
        }.launchIn(this)
        query.debounce(Debounce).onEach {
            getTitles(it, page.value)
        }.launchIn(this)
    }

    private fun getTitles(
        query: String,
        page: Int
    ) = launch {
        initLoading(viewState)
        val result = repository.getSearch(query, page)
        result.titles
            .map { it.toTitleSearch() }
            .also { list ->
                count = list.size
                if (page <= 1) {
                    list
                } else {
                    titles.value.second.toMutableList().apply {
                        addAll(list)
                    }
                }.also {
                    titles.value = query to it
                }
            }
        titleCount.emit(result.paging.total)
    }

    fun setQuery(newQuery: String) {
        query.value = newQuery
        startPage()
    }

    fun startPage() {
        page.value = Constant.START_PAGE
    }

    fun nextPage() {
        if (count == 50)
            page.value += Constant.START_PAGE
    }

    companion object {
        const val Debounce = 300L
    }
}