package net.dwgload.de.ui.collection

import android.view.View
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.TypedEpoxyController
import net.dwgload.de.R
import net.dwgload.de.data.models.collection.Collection
import net.dwgload.de.databinding.ItemCollectionBinding

class CollectionController(val onClick: (Collection) -> Unit) :
    TypedEpoxyController<List<Collection>>() {

    override fun buildModels(data: List<Collection>?) {
        data?.forEach { item ->
            CollectionModel(item).addTo(this)
        }
    }

    inner class CollectionModel(private val data: Collection) : EpoxyModel<View>() {

        init {
            id(data.hashCode())
        }

        override fun getDefaultLayout(): Int = R.layout.item_collection

        override fun bind(view: View) {
            with(ItemCollectionBinding.bind(view)) {
                titleView.text = data.name
                root.setOnClickListener {
                    onClick(data)
                }
            }
        }
    }
}