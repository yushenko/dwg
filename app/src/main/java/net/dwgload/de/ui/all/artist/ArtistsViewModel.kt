package net.dwgload.de.ui.all.artist

import androidx.hilt.lifecycle.ViewModelInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import net.dwgload.de.data.models.artist.Artist
import net.dwgload.de.data.models.artist.toArtist
import net.dwgload.de.data.repository.TitleRepository
import net.dwgload.de.shared.CoroutineViewModel
import net.dwgload.de.shared.initLoading

class ArtistsViewModel @ViewModelInject constructor(
    var repository: TitleRepository
) : CoroutineViewModel() {

    val artists = MutableStateFlow<List<Artist>>(listOf())

    init {
        loadArtist()
    }

    fun updateData() {
        loadArtist()
    }

    private fun loadArtist() = launch {
        initLoading(viewState)
        artists.value = repository.getArtists().map { it.toArtist() }
    }
}