package net.dwgload.de.ui.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import net.dwgload.de.data.repository.AudioRepository
import javax.inject.Inject

class MainViewModel @ViewModelInject constructor(var repository: AudioRepository) : ViewModel() {
    fun isNotEmptyPlayList(): Boolean {
        return !repository.getAlbum().isNullOrEmpty()
    }
}