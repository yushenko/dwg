package net.dwgload.de.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.media.session.PlaybackStateCompat
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import net.dwgload.de.R
import net.dwgload.de.data.services.PlayerService
import net.dwgload.de.ui.player.dialog.PlayerDialogFragment
import net.dwgload.de.ui.player.player.AudioController

@AndroidEntryPoint
class MainActivity :
    AppCompatActivity(R.layout.activity_main) {

    private val viewModel: MainViewModel by viewModels()

    private val callback = object : AudioController() {
        override fun getContext() = this@MainActivity

        override fun playbackStateChange(state: PlaybackStateCompat?) {
            if (state?.state == PlaybackStateCompat.STATE_PLAYING) {
//                ivPlayer.playAnimation()
            } else {
//                ivPlayer.pauseAnimation()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        ivPlayer?.setOnClickListener {
//            openPlayerFragment()
//        }

//        ivSearch?.setOnClickListener {
//
////            startActivity(Intent(this, SearchFragment::class.java))
//        }

        val navController = Navigation.findNavController(this, R.id.navHostFragment)
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        NavigationUI.setupWithNavController(bottomNavigationView, navController)

        bindService(
            Intent(this, PlayerService::class.java),
            callback.getServiceConnection(),
            Context.BIND_AUTO_CREATE
        )

        if (intent.extras?.containsKey(ARG_FROM_NOTIFICATION) == true && intent.extras?.getBoolean(
                ARG_FROM_NOTIFICATION
            ) == true
        ) {
            openPlayerFragment()
        }
    }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        ivPlayer?.setOnClickListener {
//            openPlayerFragment()
//        }
//
//        ivSearch?.setOnClickListener { startActivity(Intent(this, SearchActivity::class.java)) }
//
//        bottomNavigationView.setOnNavigationItemSelectedListener(this)
//        bottomNavigationView.selectedItemId = R.id.action_news
//        bindService(Intent(this, PlayerService::class.java), callback.getServiceConnection(), Context.BIND_AUTO_CREATE)
//
//        if (intent.extras?.containsKey(ARG_FROM_NOTIFICATION) == true && intent.extras?.getBoolean(ARG_FROM_NOTIFICATION) == true) {
//            openPlayerFragment()
//        }
//    }

    private fun openPlayerFragment() {
        if (viewModel.isNotEmptyPlayList()) {
            PlayerDialogFragment.newInstance().show(supportFragmentManager, "Player")
        }
    }

//    override fun onNavigationItemSelected(item: MenuItem): Boolean {
////        appBarAction?.visibility = if (item.itemId == R.id.action_info) View.GONE else View.VISIBLE
//
//        when (item.itemId) {
//            R.id.action_news -> {
//                if (newFragment == null) newFragment = NewsFragment()
//                changeFragment(newFragment!!, "new")
//                setScreenTitle(R.string.news)
//                return true
//            }
//            R.id.action_collections -> {
//                if (collectionFragment == null) collectionFragment = CollectionsFragment()
//                changeFragment(collectionFragment!!, "collection")
//                setScreenTitle(R.string.collections)
//                return true
//            }
//            R.id.action_all -> {
//                if (allFragment == null) allFragment = AllFragment()
//                changeFragment(allFragment!!, "all")
//                setScreenTitle(R.string.all_long)
//                return true
//            }
//            R.id.action_downloaded -> {
//                if (downloadedFragment == null) downloadedFragment = DownloadedFragment()
//                changeFragment(downloadedFragment!!, "downloaded")
//                setScreenTitle(R.string.downloaded)
//                return true
//            }
//            R.id.action_info -> {
//                if (infoFragment == null) infoFragment = InfoFragment()
//                changeFragment(infoFragment!!, "info")
//                setScreenTitle(R.string.info)
//                return true
//            }
//        }
//        return false
//    }

//    private fun changeFragment(fragment: Fragment, tagFragmentName: String) {
//
//        val mFragmentManager = supportFragmentManager
//        val fragmentTransaction = mFragmentManager.beginTransaction()
//
//        val currentFragment = mFragmentManager.primaryNavigationFragment
//        if (currentFragment != null) {
//            fragmentTransaction.hide(currentFragment)
//        }
//
//        var fragmentTemp = mFragmentManager.findFragmentByTag(tagFragmentName)
//        if (fragmentTemp == null) {
//            fragmentTemp = fragment
//            fragmentTransaction.add(R.id.flContainer, fragmentTemp, tagFragmentName)
//        } else {
//            fragmentTransaction.show(fragmentTemp)
//        }
//
//        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp)
//        fragmentTransaction.setReorderingAllowed(true)
//        fragmentTransaction.commitNowAllowingStateLoss()
//    }

    override fun onDestroy() {
        callback.disconnect()
        unbindService(callback.getServiceConnection())
        super.onDestroy()
    }

    companion object {
        private const val ARG_FROM_NOTIFICATION = "from_notification"

        fun newIntent(context: Context?, fromNotification: Boolean): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(ARG_FROM_NOTIFICATION, fromNotification)
            return intent
        }
    }
}
