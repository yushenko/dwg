package net.dwgload.de.ui.base

import android.text.Editable
import android.text.TextWatcher

interface OnTextChangeListener : TextWatcher {
    override fun afterTextChanged(p0: Editable?) {
    }

    fun textChange(text: String)

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
        textChange(text.toString())
    }

}