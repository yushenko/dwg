package net.dwgload.de.ui.all.book

import androidx.hilt.lifecycle.ViewModelInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import net.dwgload.de.data.model.Collections
import net.dwgload.de.data.models.book.Book
import net.dwgload.de.data.models.book.toBook
import net.dwgload.de.data.models.category.Category
import net.dwgload.de.data.models.category.toCategory
import net.dwgload.de.data.repository.TitleRepository
import net.dwgload.de.shared.CoroutineViewModel
import net.dwgload.de.shared.initLoading

class BooksViewModel @ViewModelInject constructor(
    private val repository: TitleRepository
) : CoroutineViewModel() {

    val books = MutableStateFlow<List<Book>>(listOf())

    init {
        loadBook()
    }

    fun updateData() {
        loadBook()
    }

    private fun loadBook() = launch {
        initLoading(viewState)
        books.value = repository.getBooks().map { it.toBook() }
    }
}