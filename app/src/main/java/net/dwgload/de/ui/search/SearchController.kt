package net.dwgload.de.ui.search

import android.view.View
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.TypedEpoxyController
import net.dwgload.de.R
import net.dwgload.de.data.models.search.TitleSearch
import net.dwgload.de.databinding.ItemSearchBinding
import net.dwgload.de.ext.setTextColorWithPath
import net.dwgload.de.shared.EmptyDataModel

class SearchController(
    val onClick: (TitleSearch) -> Unit
) : TypedEpoxyController<Pair<String, List<TitleSearch>>>() {

    override fun buildModels(data: Pair<String, List<TitleSearch>>?) {
        if (data?.second?.size == 0) EmptyDataModel().addTo(this)
        data?.second?.forEach { item ->
            DetailsModel(item, data.first).addTo(this)
        }
    }

    inner class DetailsModel(private val data: TitleSearch, private val query: String) :
        EpoxyModel<View>() {

        init {
            id(data.hashCode(), query.hashCode())
        }

        override fun getDefaultLayout(): Int = R.layout.item_search

        override fun bind(view: View) {
            with(ItemSearchBinding.bind(view)) {
                titleView.setTextColorWithPath(data.title, query)
                themeView.setTextColorWithPath(data.genres, query)
                artistView.setTextColorWithPath(data.artistName, query)
                passageView.setTextColorWithPath(data.passageBook, query)

                themeView.visibility =
                    if (data.genres.trim().isNotEmpty()) View.VISIBLE else View.GONE
                artistView.visibility =
                    if (data.artistName.trim().isNotEmpty()) View.VISIBLE else View.GONE
                passageView.visibility =
                    if (data.passageBook.isNotEmpty()) View.VISIBLE else View.GONE

                root.setOnClickListener {
                    onClick(data)
                }
            }
        }
    }
}