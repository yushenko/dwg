package net.dwgload.de.ui.all.title

import androidx.hilt.lifecycle.ViewModelInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import net.dwgload.de.data.models.all.TitleAll
import net.dwgload.de.data.models.all.toTitleAll
import net.dwgload.de.data.repository.TitleRepository
import net.dwgload.de.shared.CoroutineViewModel
import net.dwgload.de.shared.initLoading
import net.dwgload.de.shared.Constant

class TitlesViewModel @ViewModelInject constructor(
    private val repository: TitleRepository
) : CoroutineViewModel() {

    val titles = MutableStateFlow<List<TitleAll>?>(listOf())
    val page = MutableStateFlow(0)
    private var count: Int = 0


    init {
        page.onEach {
            loadTitles(it)
        }.launchIn(this)
    }

    fun startPage() {
        page.value = Constant.START_PAGE
    }

    fun nextPage() {
        if (count == 50)
            page.value += Constant.START_PAGE
    }

    fun updateData() {
        loadTitles(Constant.START_PAGE)
    }

    private fun loadTitles(page: Int) = launch {
        initLoading(viewState)
        repository.getAll(page).titles
            .map { it.toTitleAll() }
            .also { list ->
                count = list.size
                if (page <= 1) {
                    list
                } else {
                    titles.value?.toMutableList()?.apply {
                        addAll(list)
                    }
                }.also {
                    titles.value = it
                }
            }
    }
}