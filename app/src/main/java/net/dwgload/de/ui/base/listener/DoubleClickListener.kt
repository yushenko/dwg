package net.dwgload.de.ui.base.listener

import android.os.SystemClock
import android.view.View


abstract class DoubleClickListener : View.OnClickListener {
    private var doubleClickQualificationSpanInMillis: Long = 0
    private var timestampLastClick: Long = 0

    constructor() {
        doubleClickQualificationSpanInMillis = DEFAULT_QUALIFICATION_SPAN
        timestampLastClick = 0
    }

    constructor(doubleClickQualificationSpanInMillis: Long) {
        this.doubleClickQualificationSpanInMillis = doubleClickQualificationSpanInMillis
        timestampLastClick = 0
    }

    override fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() - timestampLastClick < doubleClickQualificationSpanInMillis) {
            onDoubleClick()
        }
        timestampLastClick = SystemClock.elapsedRealtime()
    }

    abstract fun onDoubleClick()

    companion object {
        private val DEFAULT_QUALIFICATION_SPAN: Long = 300
    }

}