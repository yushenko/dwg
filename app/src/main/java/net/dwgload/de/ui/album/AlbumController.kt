package net.dwgload.de.ui.album

import android.view.View
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.TypedEpoxyController
import net.dwgload.de.R
import net.dwgload.de.data.models.title.Title
import net.dwgload.de.databinding.ItemAlbumBinding

class AlbumController(
    val onClick: (Title) -> Unit
) :
    TypedEpoxyController<List<Title>>() {

    override fun buildModels(data: List<Title>?) {
        data?.forEach { item ->
            DetailsModel(item).addTo(this)
        }
    }

    inner class DetailsModel(private val data: Title) : EpoxyModel<View>() {

        init {
            id(data.hashCode())
        }

        override fun getDefaultLayout(): Int = R.layout.item_album

        override fun bind(view: View) {
            with(ItemAlbumBinding.bind(view)) {
                titleView.text =
                    root.context.getString(R.string.title_item_number, data.track, data.mainTitle)
                themeView.text = data.genres
                artistView.text = data.artistName
                passageView.text = data.passageBook

                themeView.visibility =
                    if (data.genres.trim().isNotEmpty()) View.VISIBLE else View.GONE
                artistView.visibility =
                    if (data.artistName.trim().isNotEmpty()) View.VISIBLE else View.GONE
                passageView.visibility =
                    if (data.passageBook.isNotEmpty()) View.VISIBLE else View.GONE

                root.setOnClickListener {
                    onClick(data)
                }

            }
        }
    }
}