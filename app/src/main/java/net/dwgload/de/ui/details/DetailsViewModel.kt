package net.dwgload.de.ui.details

import androidx.hilt.lifecycle.ViewModelInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import net.dwgload.de.data.models.title.Title
import net.dwgload.de.data.models.title.toTitle
import net.dwgload.de.data.repository.TitleRepository
import net.dwgload.de.shared.CoroutineViewModel
import net.dwgload.de.shared.initLoading
import net.dwgload.de.shared.query.QueryParam
import net.dwgload.de.shared.Constant

class DetailsViewModel @ViewModelInject constructor(
    private val repository: TitleRepository
) : CoroutineViewModel() {

    val titles = MutableStateFlow<List<Title>?>(listOf())
    val page = MutableStateFlow(0)
    private var count: Int = 0
    private var type: QueryParam? = null
    private var id: Int? = null

    init {
        page.onEach {
            loadTitles(it, type, id)
        }.launchIn(this)
    }

    fun startPage(type: QueryParam, id: Int) {
        this.type = type
        this.id = id
        page.value = Constant.START_PAGE
    }

    fun nextPage() {
        if (count == 50)
            page.value += Constant.START_PAGE
    }

    private fun loadTitles(page: Int, type: QueryParam?, id: Int?) = launch {
        initLoading(viewState)
        repository.getDetails(type, page, id).titles
            .map { it.toTitle() }
            .also { list ->
                count = list.size
                if (page <= 1) {
                    list
                } else {
                    titles.value?.toMutableList()?.apply {
                        addAll(list)
                    }
                }.also {
                    titles.value = it
                }
            }
    }
}