package net.dwgload.de.ui.downloaded

import androidx.hilt.lifecycle.ViewModelInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import net.dwgload.de.data.models.title.Title
import net.dwgload.de.shared.CoroutineViewModel

class DownloadedViewModel @ViewModelInject constructor(
//    var appDatabase: AppDatabase,
//    var repository: AudioRepository
) : CoroutineViewModel() {

    val album = MutableStateFlow<List<Title>>(listOf())

    init {
        launch {
            album.value = listOf()
        }
    }
}