package net.dwgload.de.ui.search

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.idapgroup.lifecycle.ktx.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import net.dwgload.de.R
import net.dwgload.de.databinding.FragmentSearchBinding
import net.dwgload.de.ext.addScrollListener
import net.dwgload.de.shared.BaseFragment
import net.dwgload.de.shared.StateProgressLoading
import net.dwgload.de.shared.ViewState

@AndroidEntryPoint
class SearchFragment : BaseFragment<FragmentSearchBinding>(R.layout.fragment_search) {

    private val viewModel: SearchViewModel by viewModels()

    private val controller = SearchController {
        hideKeyboard()
        findNavController().navigate(
            SearchFragmentDirections.openPlayerFragment(it.title, it.albumId, it.id)
        )
    }

    override fun FragmentSearchBinding.onInitViews() {
        recyclerView.adapter = controller.adapter
        recyclerView.addScrollListener {
            viewModel.nextPage()
        }
        backView.setOnClickListener {
            hideKeyboard()
            findNavController().popBackStack()
        }
        searchView.changed = {
            viewModel.setQuery(it)
        }
        viewModel.startPage()
    }

    override fun FragmentSearchBinding.onInitObserves() {
        with(viewModel) {
            observe(viewState) {
                if (it is ViewState.Loading) searchView.stateLoading = StateProgressLoading.Loading
                else searchView.stateLoading = StateProgressLoading.Successful
            }
            titles.filterNotNull().onEach {
                controller.setData(it)
            }.launchIn(viewLifecycleOwner.lifecycleScope)
            titleCount.filterNotNull().onEach {
                showTitleCount(it)
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }
    }

    private fun FragmentSearchBinding.showTitleCount(count: Int) {
        layoutCount.visibility = if (count > 0) View.VISIBLE else View.GONE
        countView.text = count.toString()
    }

    private fun hideKeyboard() {
        val inputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    override fun initViewBinding(view: View) = FragmentSearchBinding.bind(view)
}