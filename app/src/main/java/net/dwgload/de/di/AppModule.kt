package net.dwgload.de.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import net.dwgload.de.data.network.ApiRestService
import net.dwgload.de.data.network.Retrofit
import retrofit2.Retrofit

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Provides
    fun provideRetrofit(): Retrofit = Retrofit()


    @Provides
    fun provideApiRestService(retrofit: Retrofit): ApiRestService = retrofit.create(ApiRestService::class.java)

}