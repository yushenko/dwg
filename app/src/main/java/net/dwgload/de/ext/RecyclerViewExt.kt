package net.dwgload.de.ext

import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.setupVerticalDivider() {
    addItemDecoration(
        DividerItemDecoration(
            context,
            RecyclerView.VERTICAL
        )
    )
}

fun RecyclerView.addScrollListener(
    onLoadMoreListener: (Int) -> Unit
) {
    val linearLayoutManager = this.layoutManager as? LinearLayoutManager
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if(!recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN)) {
                val visibleItemCount = linearLayoutManager?.childCount ?: 0
                val totalItemCount = linearLayoutManager?.itemCount ?: 0
                val pastVisibleItems = linearLayoutManager?.findFirstVisibleItemPosition() ?: 0
                if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                    onLoadMoreListener.invoke(totalItemCount)
                }
            }
        }
    })

}