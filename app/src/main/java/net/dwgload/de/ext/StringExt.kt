package net.dwgload.de.ext

import org.jsoup.Jsoup

fun String.parseHtml(): String {
    val list = Jsoup.parse(this, "UTF-8").select("a[href]")
    var text = ""
    for (i in 0 until list.size) {
        text += list[i].text()
        if (i < list.size - 1) {
            text += ", "
        }
    }
    return text
}