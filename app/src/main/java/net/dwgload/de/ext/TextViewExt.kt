package net.dwgload.de.ext

import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.TextView

fun TextView.setTextColorWithPath(text: String, path: String) {

    val result = SpannableString(text)
    val occurrenceIndex: Int = text.indexOf(path, 0, true)
    if (occurrenceIndex >= 0) {
        result.setSpan(
            ForegroundColorSpan(Color.RED),
            occurrenceIndex,
            occurrenceIndex + path.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    setText(result)
}