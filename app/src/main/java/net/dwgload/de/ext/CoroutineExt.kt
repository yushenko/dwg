package net.dwgload.de.ext

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job

fun CoroutineScope.doFinally(action: (it: Throwable?) -> Unit) {
    coroutineContext[Job]?.invokeOnCompletion { action(it) }
}