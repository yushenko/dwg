package net.dwgload.de.data.responce.title


import com.google.gson.annotations.SerializedName

data class GenreResponse(
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("num_titles")
    val numTitles: Int,
    @SerializedName("updated_at")
    val updatedAt: String
)