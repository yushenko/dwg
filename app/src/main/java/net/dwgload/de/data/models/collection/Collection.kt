package net.dwgload.de.data.models.collection

import net.dwgload.de.data.responce.collection.CollectionResponse

data class Collection(
    val id: Int,
    val name: String,
    val count: Int
)

fun CollectionResponse.toCollection() = Collection(
    id = id,
    name = name,
    count = pos
)