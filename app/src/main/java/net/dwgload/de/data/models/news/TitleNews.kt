package net.dwgload.de.data.models.news

import net.dwgload.de.data.responce.title.TitleResponse
import net.dwgload.de.ext.parseHtml

data class TitleNews (
    val id: Int,
    var title: String,
    var artistName: String,
    var genres: String,
    var titlesCount: Int,
    var passageBook: String,
    var albumId: Int
)

fun TitleResponse.toTitleNews() = TitleNews(
    id = id,
    title = album.name,
    artistName = artist.name,
    genres = if(genres.isNullOrEmpty()) genre.parseHtml() else genres.joinToString(", ") { it.name },
    titlesCount = album.numTitles,
    passageBook = if(passages.isNullOrEmpty()) "" else passages.joinToString(separator = "; ") { it.passageBook.long.plus(" ").plus(it.chapter) },
    albumId = albumId
)
