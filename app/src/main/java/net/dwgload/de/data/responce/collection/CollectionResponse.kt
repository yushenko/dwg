package net.dwgload.de.data.responce.collection

import com.google.gson.annotations.SerializedName

data class CollectionResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("pos")
    val pos: Int,
    @SerializedName("created_at")
    val created_at: String,
    @SerializedName("updated_at")
    val updated_at: String
)