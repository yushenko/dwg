package net.dwgload.de.data.responce.title


import com.google.gson.annotations.SerializedName

data class TitlesResponse(
    @SerializedName("paging")
    val paging: PagingResponse,
    @SerializedName("titles")
    val titles: List<TitleResponse>
)