package net.dwgload.de.data.responce.title


import com.google.gson.annotations.SerializedName

data class PagingResponse(
    @SerializedName("page")
    val page: Int,
    @SerializedName("titles_per_page")
    val titlesPerPage: Int,
    @SerializedName("total")
    val total: Int
)