package net.dwgload.de.data.models.all

import net.dwgload.de.data.responce.title.TitleResponse
import net.dwgload.de.ext.parseHtml

data class TitleAll(
    val id: Int,
    val title: String,
    val artistName: String,
    val genres: String,
    val track: Int,
    val passageBook: String,
    val albumId: Int
)

fun TitleResponse.toTitleAll() = TitleAll(
    id = id,
    title = title,
    artistName = artist.name,
    genres = if (genres.isNullOrEmpty()) genre.parseHtml() else genres.joinToString(", ") { it.name },
    track = track,
    passageBook = passage ?: "",
    albumId = albumId
)
