package net.dwgload.de.data.models.player

import net.dwgload.de.data.model.FormatMedia
import net.dwgload.de.data.model.TitleView
import net.dwgload.de.data.responce.title.ArtistResponse
import net.dwgload.de.data.responce.title.TitleResponse
import net.dwgload.de.ext.parseHtml

data class TitlePlayer (
    val selected: Boolean = false,
    val id: Int,
    val mainTitle: String,
    val artistName: String,
    val genres: String,
    val titlesCount: Int,
    val passageBook: String,
    val albumId: Int,
    val playtimeInMillis: Long,
    val albumTitle: String,
    val year: String,
    val url: String,
    val downloadUrl: String,
    val downloadable: Boolean,
    val formatMedia: FormatMedia,
    val track: Int,
    val fileName: String
)

fun TitleResponse.toTitlePlayer() = TitlePlayer(
    id = id,
    mainTitle = if(!title.isNullOrEmpty()) title else album.name,
    artistName = artist.name,
    genres = if(genres.isNullOrEmpty()) genre.parseHtml() else genres.joinToString(", ") { it.name },
    titlesCount = album.numTitles,
    passageBook = if(passages.isNullOrEmpty()) "" else passages.joinToString(separator = "; ") { it.passageBook.long.plus(" ").plus(it.chapter) },
    albumId = albumId,
    playtimeInMillis = (playtime.toInt() * 1000).toLong(),
    albumTitle = (album.name) + " " + track + "/" + (album.numTitles ?: ""),
    year = year,
    url = url,
    downloadUrl = downloadUrl,
    downloadable = downloadable,
    track = track,
    formatMedia = if (url.contains("mp4")) FormatMedia.VIDEO else FormatMedia.AUDIO,
    fileName = filename
)

fun TitlePlayer.toTitleView() = TitleView(
    id = id,
    mainTitle = mainTitle,
    artistName = artistName,
    genres = genres,
    titlesCount = titlesCount,
    passageBook = passageBook,
    albumId = albumId,
    playtimeInMillis = playtimeInMillis,
    albumTitle = albumTitle,
    year = year,
    url = url,
    downloadUrl = downloadUrl,
    downloadable = downloadable,
    track = track,
    formatMedia = if (url.contains("mp4")) FormatMedia.VIDEO else FormatMedia.AUDIO,
    fileName = fileName
)

fun List<TitleResponse>.toTitlesPlayer(titleId: Int) = map {
    with(it) {
        TitlePlayer(
            selected = titleId == id,
            id = id,
            mainTitle = if(!title.isNullOrEmpty()) title else album.name,
            artistName = artist.name,
            genres = if(genres.isNullOrEmpty()) genre.parseHtml() else genres.joinToString(", ") { it.name },
            titlesCount = album.numTitles,
            passageBook = if(passages.isNullOrEmpty()) "" else passages.joinToString(separator = "; ") { it.passageBook.long.plus(" ").plus(it.chapter) },
            albumId = albumId,
            playtimeInMillis = (playtime.toInt() * 1000).toLong(),
            albumTitle = (album.name) + " " + track + "/" + (album.numTitles ?: ""),
            year = year,
            url = url,
            downloadUrl = downloadUrl,
            downloadable = downloadable,
            track = track,
            formatMedia = if (url.contains("mp4")) FormatMedia.VIDEO else FormatMedia.AUDIO,
            fileName = filename
        )
    }
}

fun List<TitleResponse>.toTracks(titleId: Int): List<TitlePlayer> =
    sortedBy { it.track }.toTitlesPlayer(titleId)
