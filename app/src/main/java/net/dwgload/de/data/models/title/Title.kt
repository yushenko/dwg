package net.dwgload.de.data.models.title

import net.dwgload.de.data.model.FormatMedia
import net.dwgload.de.data.responce.title.ArtistResponse
import net.dwgload.de.data.responce.title.TitleResponse
import net.dwgload.de.ext.parseHtml

data class Title (
    val id: Int,
    var mainTitle: String,
    var artistName: String,
    var genres: String,
    var titlesCount: Int,
    var passageBook: String,
    var albumId: Int,
    var playtimeInMillis: Long,
    var albumTitle: String,
    var year: String,
    var url: String,
    val downloadUrl: String,
    val downloadable: Boolean,
    var formatMedia: FormatMedia,
    var track: Int,
    var fileName: String,
    var needShowTitleCount: Boolean,
    var isNew: Boolean
)

fun TitleResponse.toTitle() = Title(
    id = id,
    mainTitle = if(!title.isNullOrEmpty()) title else album.name,
    artistName = artist.name,
    genres = if(genres.isNullOrEmpty()) genre.parseHtml() else genres.joinToString(", ") { it.name },
    titlesCount = album.numTitles,
    passageBook = if(passages.isNullOrEmpty()) "" else passages.joinToString(separator = "; ") { it.passageBook.long.plus(" ").plus(it.chapter) },
    albumId = albumId,
    playtimeInMillis = (playtime.toInt() * 1000).toLong(),
    albumTitle = (album.name) + " " + track + "/" + (album.numTitles ?: ""),
    year = year,
    url = url,
    downloadUrl = downloadUrl,
    downloadable = downloadable,
    track = track,
    formatMedia = if (url.contains("mp4")) FormatMedia.VIDEO else FormatMedia.AUDIO,
    fileName = filename,
    isNew = new,
    needShowTitleCount = true
)
