package net.dwgload.de.data.models.artist

import net.dwgload.de.data.responce.title.ArtistResponse

data class Artist(
    val id: Int,
    val image: String,
    val name: String,
    val count: Int
)

fun ArtistResponse.toArtist() = Artist(
    id = id,
    image = image ?: "",
    name = name,
    count = numTitles
)