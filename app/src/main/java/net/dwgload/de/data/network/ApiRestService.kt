package net.dwgload.de.data.network

import net.dwgload.de.data.responce.collection.CollectionResponse
import net.dwgload.de.data.responce.title.ArtistResponse
import net.dwgload.de.data.responce.title.BookResponse
import net.dwgload.de.data.responce.title.GenreResponse
import net.dwgload.de.data.responce.title.TitlesResponse
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap


interface ApiRestService {

    @GET("title")
    suspend fun getTitles(@QueryMap options: @JvmSuppressWildcards Map<String, Any>): TitlesResponse

    @GET("title?")
    suspend fun getDetails(@QueryMap options: @JvmSuppressWildcards Map<String, Any>): TitlesResponse

    @GET("title?")
    suspend fun getAlbum(@QueryMap options: @JvmSuppressWildcards Map<String, Any>): TitlesResponse

    @GET("title?")
    suspend fun getSearchByQuery(@Query("q") query: String, @Query("p") page: Int?): TitlesResponse

    @GET("title?p=1&n=1")
    suspend fun getNews(): TitlesResponse

    @GET("all?s=year")
    suspend fun getAll(@Query("p") page: Int): TitlesResponse

    @GET("artist")
    suspend fun getArtists(): List<ArtistResponse>

    @GET("genre")
    suspend fun getGenres(): List<GenreResponse>

    @GET("book")
    suspend fun getBook(): List<BookResponse>

    @GET("collection")
    suspend fun getCollections(): List<CollectionResponse>
}