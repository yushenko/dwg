package net.dwgload.de.data.models.category

import net.dwgload.de.data.responce.title.GenreResponse

data class Category(
    val id: Int,
    val name: String,
    val count: Int
)

fun GenreResponse.toCategory() = Category(
    id = id,
    name = name,
    count = numTitles
)