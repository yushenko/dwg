package net.dwgload.de.data.responce.title


import com.google.gson.annotations.SerializedName

data class PassageResponse(
    @SerializedName("chapter")
    val chapter: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("num_titles")
    val numTitles: String,
    @SerializedName("PassageBook")
    val passageBook: BookResponse,
    @SerializedName("passage_book_id")
    val passageBookId: String,
    @SerializedName("updated_at")
    val updatedAt: String
)