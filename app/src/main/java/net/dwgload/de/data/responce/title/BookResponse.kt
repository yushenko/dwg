package net.dwgload.de.data.responce.title


import com.google.gson.annotations.SerializedName

data class BookResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("long")
    val long: String,
    @SerializedName("short")
    val short: String,
    @SerializedName("num_titles")
    val numTitles: Int
)