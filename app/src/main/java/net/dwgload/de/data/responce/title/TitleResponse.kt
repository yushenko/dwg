package net.dwgload.de.data.responce.title


import com.google.gson.annotations.SerializedName

data class TitleResponse(
    @SerializedName("Album")
    val album: AlbumResponse,
    @SerializedName("album_id")
    val albumId: Int,
    @SerializedName("album_playtime")
    val albumPlaytime: Int,
    @SerializedName("Artist")
    val artist: ArtistResponse,
    @SerializedName("artist_id")
    val artistId: String,
    @SerializedName("bitrate")
    val bitrate: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("date")
    val date: String,
    @SerializedName("date_last_download")
    val dateLastDownload: String,
    @SerializedName("date_last_play")
    val dateLastPlay: String,
    @SerializedName("download_series")
    val downloadSeries: String,
    @SerializedName("download_url")
    val downloadUrl: String,
    @SerializedName("downloadable")
    val downloadable: Boolean,
    @SerializedName("filedate")
    val filedate: String,
    @SerializedName("filename")
    val filename: String,
    @SerializedName("Genres")
    val genres: List<GenreResponse>,
    @SerializedName("genres")
    val genre: String,
    @SerializedName("groupalbum")
    val groupalbum: Boolean,
    @SerializedName("hash")
    val hash: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("new")
    val new: Boolean,
    @SerializedName("num_downloaded")
    val numDownloaded: String,
    @SerializedName("num_played")
    val numPlayed: String,
    @SerializedName("Passages")
    val passages: List<PassageResponse>,
    @SerializedName("passages")
    val passage: String,
    @SerializedName("playtime")
    val playtime: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("track")
    val track: Int,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("year")
    val year: String
)