package net.dwgload.de.data.repository

import net.dwgload.de.data.network.ApiRestService
import net.dwgload.de.shared.Constant.START_PAGE
import net.dwgload.de.shared.query.QueryParam
import javax.inject.Inject

class TitleRepository @Inject constructor(
        private val apiRestService: ApiRestService
) {
    suspend fun getTitles(type: QueryParam, page: Int?, id: Int?) = apiRestService.getTitles(setParam(id, type, page))

    suspend fun getAlbum(type: QueryParam?, page: Int?, id: Int?) = apiRestService.getAlbum(setParam(id, type, page))

    suspend fun getDetails(type: QueryParam?, page: Int?, id: Int?) = apiRestService.getDetails(setParam(id, type, page))

    suspend fun getSearch(query: String, page: Int?) = apiRestService.getSearchByQuery(query, page)

    suspend fun getNews() = apiRestService.getNews()

    suspend fun getAll(page: Int) = apiRestService.getAll(page)

    suspend fun getArtists() = apiRestService.getArtists()

    suspend fun getCategories() = apiRestService.getGenres()

    suspend fun getBooks() = apiRestService.getBook()

    suspend fun getCollections() = apiRestService.getCollections()


    private fun setParam(id: Int?, type: QueryParam?, page: Int?): HashMap<String, Any> {
        val map = hashMapOf<String, Any>()
        if (id != null && type != null) map[type.query] = id
        map[QueryParam.Page.query] = page ?: START_PAGE
        return map
    }
}
