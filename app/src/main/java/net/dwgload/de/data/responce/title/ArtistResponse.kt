package net.dwgload.de.data.responce.title


import com.google.gson.annotations.SerializedName

data class ArtistResponse(
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("image")
    val image: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("num_titles")
    val numTitles: Int,
    @SerializedName("updated_at")
    val updatedAt: String
)