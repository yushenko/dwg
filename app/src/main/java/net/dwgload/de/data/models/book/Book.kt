package net.dwgload.de.data.models.book

import net.dwgload.de.data.responce.title.BookResponse


data class Book(
    val id: Int,
    val name: String,
    val count: Int
)

fun BookResponse.toBook() = Book(
    id = id,
    name = long,
    count = numTitles
)